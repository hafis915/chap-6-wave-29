const { Student, Country, CapitalCity , Citizen, Product, Chart } = require('../models')
const db = require('../models')
class UserController {

    /**
        Create V
        Read V
        Update V
        Delete V

     */
    static async getAllStudents(req,res) {
        try {
            const data = await Student.findAll()
            console.log(data)
            res.status(200).json({
                data 
            })
        } catch (error) {
            res.status(400).json({
                msg : "error"
            })
        }
    }

    static async addStudent(req,res) {
        try {
            const { name, age ,address, fullName } = req.body
            const payload = {name , age ,address, fullName}

            const newStudent = await Student.create(payload)

            res.status(200).json({
                data : newStudent
            })


        } catch (error) {
            console.log(error)
            res.status(400).json({
                msg : "error"
            })
        }
    }

    static async updateStudent(req,res) {
        try {
            const { name, age ,address, fullName } = req.body
            const id = req.params.id
            const payload = {name , age ,address, fullName}

            const data = await Student.update(payload, {
                where : {
                    id : id
                },
                returning : true
            })

            if(!data[0]) {
                return res.status(404).json({
                    msg : 'Data Not found'
                })
            }

            res.status(200).json(
               { data : data}
            )
        } catch (error) {
            res.status(400).json({
                msg : "error"
            })
        }
    }

    static async deleteStudent(req,res) {
        try {
            const id = req.params.id

            const data = await Student.destroy({
                where : {
                    id
                },
            })
            if(!data) {
                return res.status(404).json({
                    msg : 'Data Not found'
                })
            }


            res.status(200).json({data})
        } catch (error) {
            res.status(400).json({
                msg : "error"
            })
        }
    }

    static async createCountry(req,res) {
        try {
            const { name } = req.body
            const data = await Country.create({
                name, independenceDate : new Date()
            })

            return res.status(200).json({
                data
            })
        } catch (error) {
            console.log(error)
            res.status(400).json({
                msg : "error"
            })
        }
    }

    static async getCountry(req,res) {
        try {
            const id = req.params.id
            const country = await Country.findOne( {
                where : {id},
                include : [
                    {
                        model : CapitalCity
                    },
                    {
                        model : Citizen
                    }
                ]
            })

            return res.status(200).json({
                data : country
            })

        } catch (error) {
            res.status(400).json({
                msg : "error"
            })
        }
    }

    static async getCountries(req,res) {
        try {
            const id = req.params.id
            const country = await Country.findAll( {
                include : [
                    {
                        model : CapitalCity,
                        required : true
                    },
                    {
                        model : Citizen,
                        required : true
                    }
                ]
            })

            return res.status(200).json({
                data : country
            })

        } catch (error) {
            res.status(400).json({
                msg : "error"
            })
        }
    }

    static async createCapital(req,res) {
        try {
            const { name, countryId} = req.body
            const data = await CapitalCity.create({name , countryId})
            return res.status(200).json({data})
        } catch (error) {
            res.status(400).json({
                msg : "error"
            })
        }
    }

    static async getCapital(req,res) {
        try {
            const id = req.params.id
            const capital = await CapitalCity.findOne( {
                where : {id},
                include : [
                    {
                        model : Country
                    }
                ]
            })

            return res.status(200).json({data : capital})
            
        } catch (error) {
            res.status(400).json({
                msg : "error"
            })
        }
    }

    static async getCitizens(req,res) {
        try {
            /**
             {
                where : {},

             } 
             
             */
            const citizens = await Citizen.findAll({
                attributes : ['name'],
                include : [
                    {
                        model : Country,
                        attributes : ['name']
                    },
                    {
                        model : Product,
                        attributes : ['name', 'price']
                    },
                    {
                        model : Chart,
                        as : 'keranjang'
                    }
                ]
            }) 

            console.log(citizens[0].toJSON(), "<< ")

            return res.status(200).json({ data : citizens})
        } catch (error) {
            console.log(error)
            res.status(400).json({
                msg : "error"
            })
        }
    }

    static async createProduct(req,res) {
        try {
            const { name, price } = req.body
            const newProduct = await Product.create({name, price})
            res.status(200).json({
                data : newProduct
            })
        } catch (error) {
            res.status(400).json({
                msg : "error"
            })
        }
    }

    static async createChart(req,res) {
        try {
            const {productId, userId}  = req.body
            const chart = await Chart.create({productId, userId})

            res.status(200).json({
                data : chart
            })
        } catch (error) {
            res.status(400).json({
                msg : "error"
            })
        }
    }

    static async getCharts(req,res) {
        try {
            const chart = await Chart.findAll({
                include : [
                    {
                        model : Product
                    },
                    {
                        model : Citizen
                    }
                ]
            }) 
            res.status(200).json({
                data : chart
            })
        } catch (error) {
            console.log(error)
            res.status(400).json({
                msg : "error"
            })
        }
    }

    static async getProducts(req,res) {
        try {
            const product = await Product.findAll({
                include : [
                    {
                        model : Chart
                    }
                ]
            })
            res.status(200).json({
                data : product
            })
        } catch (error) {
            console.log(error)
            res.status(400).json({
                msg : "error"
            })
        }
    }




}

module.exports = UserController