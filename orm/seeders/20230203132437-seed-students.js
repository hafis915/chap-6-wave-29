'use strict';

/** @type {import('sequelize-cli').Migration} */

const students = require('./students.json').students

module.exports = {
  async up (queryInterface, Sequelize) {
    students.forEach(el => {
      el.createdAt = new Date(),
      el.updatedAt = new Date()
    })
    await queryInterface.bulkInsert('Students', students, {})
  },

  async down (queryInterface, Sequelize) {
    await queryInterface.bulkDelete('Students', null, {})
  }
};
