const express = require('express')
const router = express.Router()
const UserController = require("../controller")


router.get("/students", UserController.getAllStudents)
router.post("/students", UserController.addStudent)
router.put("/students/:id", UserController.updateStudent)
router.delete("/students/:id", UserController.deleteStudent)

router.post('/country', UserController.createCountry)
router.get('/country/:id', UserController.getCountry)
router.get('/country', UserController.getCountries)
router.post('/capital', UserController.createCapital)
router.get('/capital/:id', UserController.getCapital)

router.get('/citizens', UserController.getCitizens)

router.post('/product', UserController.createProduct)
router.get('/product', UserController.getProducts)


router.post('/chart', UserController.createChart)
router.get('/chart', UserController.getCharts)



module.exports = router 