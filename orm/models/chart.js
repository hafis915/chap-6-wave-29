'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Chart extends Model {

    static associate(models) {
      Chart.belongsTo(models.Product, {foreignKey : 'productId'})
      Chart.belongsTo(models.Citizen, {foreignKey : 'userId'})
    }
  }
  Chart.init({
    productId: DataTypes.INTEGER,
    userId: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'Chart',
  });
  return Chart;
};