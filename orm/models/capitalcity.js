'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class CapitalCity extends Model {
    static associate(models) {
      CapitalCity.belongsTo(models.Country, {foreignKey  : 'countryId'})
    }
  }
  CapitalCity.init({
    name: DataTypes.STRING,
    countryId: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'CapitalCity',
  });
  return CapitalCity;
};