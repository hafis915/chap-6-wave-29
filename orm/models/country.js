'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Country extends Model {
    static associate(models) {
      Country.hasOne(models.CapitalCity, { foreignKey : 'countryId' })
      Country.hasMany(models.Citizen, { foreignKey : 'countryId' })
    }
  }
  Country.init({
    name: DataTypes.STRING,
    independenceDate: DataTypes.DATE
  }, {
    sequelize,
    modelName: 'Country',
  });
  return Country;
};