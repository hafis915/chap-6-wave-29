'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Citizen extends Model {
    static associate(models) {
      Citizen.belongsTo(models.Country, {foreignKey : 'countryId'})
      Citizen.belongsToMany(models.Product, {through : 'Charts', foreignKey : 'userId'})
      Citizen.hasMany(models.Chart, { foreignKey : 'userId', as : 'keranjang'})
    }
  }
  Citizen.init({
    name: DataTypes.STRING,
    countryId: DataTypes.INTEGER,
    address: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'Citizen',
  });
  return Citizen;
};