'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Product extends Model {
    static associate(models) {
      Product.belongsToMany(models.Citizen, { through : 'Charts', foreignKey : 'productId'})
      Product.hasMany(models.Chart, {foreignKey : 'productId'})
    }
  }
  Product.init({
    name: DataTypes.STRING,
    price: DataTypes.INTEGER
  }, {
    sequelize,
    modelName: 'Product',
  });
  return Product;
};